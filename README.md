#Load Balancer
Installs [Apache httpd](https://httpd.apache.org/) and configures it to run as a load balancer.

## Required Variables
- load_balancer_host - Example: 192.168.10.4
- balancers - Configuration of balancers, see the example below.

Example of balancers configuration:

balancers:                              # multiple balancers can be defined here analogically to balancer1
  balancer1:
    load_balancer_proxy_requests: "off" # controls Apache forward proxy, do not enable unless you have <Proxy> control block defined
    load_balancer_port: 8181            # port on which the load balancer will be accessed
    nodes:
      node1:
        url: 192.168.0.12               # node url i.e. 192.168.0.10
        port: 8080                      # port on which the node is listening
      node2:
        url: 192.168.0.12               # node url i.e. 192.168.0.11
        port: 8080                      # port on which the node is listening
  balancer2:
    load_balancer_proxy_requests: "off"
    load_balancer_port: 8282
    nodes:
      node1:
        url: 192.168.0.12
        port: 8080
      node2:
        url: 192.168.0.13
        port: 8080

## Optional
- load_balancer_method - Possible values bybusyness/bytraffic/byrequests/heartbeat. Defaults to byrequests.

##License
Apache License, Version 2.0

## Author Information

Zawisza Hodzic
